package inrofview;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.Video;
import org.opencv.videoio.VideoCapture;

import java.io.IOException;

/**
 * Created by jimmy on 2015/06/06.
 */
public class VideoLoader {
    private VideoCapture videoCapture;

    public VideoLoader() throws IOException {
        this(0);
    }
    public VideoLoader(int device) throws IOException {
        videoCapture = new VideoCapture(device);
        if (!videoCapture.isOpened()) throw new IOException("Target camera can't open");
    }

    public Mat getImage(){
        Mat frame;
        if (videoCapture.isOpened()){
            frame = new Mat();
            videoCapture.read(frame);
            return  frame;
        }
        return null;
    }

    public void dispose(){
        videoCapture.release();
        videoCapture = null;
    }
}
