package inrofview;

import gnu.io.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.*;

/**
 * Created by jimmy on 2015/06/12.
 */
public class SerialManager implements SerialPortEventListener{
    private SerialPort port;
    private BufferedReader reader;

    private List<String> recievedMessages = new ArrayList<String>();

    public SerialManager(String portname){
        try {
            CommPortIdentifier portID = CommPortIdentifier.getPortIdentifier(portname);
            port = (SerialPort)portID.open("SerialManager",2000);
            port.setSerialPortParams(
                    9600,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE
            );
            port.setFlowControlMode(SerialPort.FLOWCONTROL_NONE);
            port.setDTR(true);

            reader = new BufferedReader(new InputStreamReader(port.getInputStream()));

            port.addEventListener(this);
            port.notifyOnDataAvailable(true);
        }
        catch (NoSuchPortException e){
            e.printStackTrace();
        }
        catch (PortInUseException e){
            e.printStackTrace();
        }
        catch (UnsupportedCommOperationException e){
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        } catch (TooManyListenersException e) {
            e.printStackTrace();
        }
    }

    public static Map<String,Boolean> getPorts(){
        Enumeration portList = CommPortIdentifier.getPortIdentifiers();
        CommPortIdentifier portIdentifier;
        Map<String,Boolean> ports = new HashMap<>();

        while (portList.hasMoreElements()){
            portIdentifier = (CommPortIdentifier)portList.nextElement();
            if(portIdentifier.getPortType() == CommPortIdentifier.PORT_SERIAL){
                ports.put(portIdentifier.getName(), portIdentifier.isCurrentlyOwned());
            }
        }
        return ports;
    }

    public void sendMessage(String message){
        String line = message+"\n";
        OutputStream output;
        try {
            byte[] bytes = line.getBytes();

            output = port.getOutputStream();
            output.write(bytes);
            output.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String[] getRecievedMessages(){
        String[] array = (String[])(recievedMessages.toArray(new String[0]));
        recievedMessages = new ArrayList<String>();

        return array;
    }

    @Override
    public void serialEvent(SerialPortEvent serialPortEvent) {
        if (serialPortEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE){
            try {
                while (reader.ready()){
                    recievedMessages.add(reader.readLine());
                }
            }
            catch (IOException e){
                e.printStackTrace();
            }
        }
    }
    public void close(){
        port.close();
    }

    public boolean isOpened(){
        if (port == null) return false;
        else return true;
    }
}
