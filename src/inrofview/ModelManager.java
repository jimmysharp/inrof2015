package inrofview;

import  com.interactivemesh.jfx.importer.obj.ObjModelImporter;
import com.sun.javafx.geom.transform.Affine3D;
import com.sun.javafx.geom.transform.BaseTransform;
import javafx.scene.*;
import javafx.scene.image.WritableImage;
import javafx.scene.media.Track;
import javafx.scene.paint.Color;
import javafx.scene.shape.MeshView;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Transform;
import javafx.scene.transform.Translate;

import java.net.URL;

/**
 * Created by jimmy on 2015/06/06.
 */
public class ModelManager {
    private Group root;
    private Scene modelScene;

    private MeshView inrofmesh[];
    private Group inrofmodel;
    private PerspectiveCamera selfViewCamera;
    private PerspectiveCamera topViewCamera;

    private Position position;
    private double cam_elevation;
    private double cam_height;

    private Translate selfViewTranslate;
    private Rotate selfViewRotateX;
    private Rotate selfViewRotateY;
    private Rotate selfViewRotateZ;

    public ModelManager(URL modelURL, double cam_fov, double cam_height, double cam_elevation, double width, double height){
        this.cam_elevation = cam_elevation;
        this.cam_height = cam_height;
        root = new Group();
        ObjModelImporter importer = new ObjModelImporter();
        importer.read(modelURL);

        inrofmesh = importer.getImport();
        importer.close();
        inrofmodel = new Group();
        inrofmodel.getChildren().addAll(inrofmesh);

        //InRof台モデルの軸合わせ
        //スタート台を下にして左下原点，右がx，上がy，手前z
        inrofmodel.getTransforms().add(new Rotate(180,Rotate.Y_AXIS));
        inrofmodel.getTransforms().add(new Rotate(90,Rotate.Z_AXIS));
        inrofmodel.getTransforms().add(new Translate(636-12,1800-198+12));

        root.getChildren().add(inrofmodel);

        //ロボット視点カメラ初期設定
        selfViewCamera = new PerspectiveCamera(true);
        selfViewCamera.setFieldOfView(cam_fov);
        selfViewCamera.setFarClip(1_000_000.0);

        selfViewTranslate = new Translate(300,500,cam_height);
        selfViewRotateY = new Rotate(180,Rotate.Y_AXIS);
        selfViewRotateZ = new Rotate(180, Rotate.Z_AXIS);
        selfViewRotateX = new Rotate(cam_elevation, Rotate.X_AXIS);
        selfViewCamera.getTransforms().addAll(selfViewTranslate,selfViewRotateY,selfViewRotateZ,selfViewRotateX);
        setSelfViewPosition(new Position(250, 500 - 230, 90));

        //ライト設定
        AmbientLight ambient = new AmbientLight(new Color(0.2, 0.2, 0.2, 0.5));
        root.getChildren().add(ambient);
        PointLight pointLight = new PointLight(new Color(0.4,0.4,0.4,0.6));
        pointLight.setTranslateX(0);
        pointLight.setTranslateY(0);
        pointLight.setTranslateZ(2000);
        root.getChildren().add(pointLight);
        PointLight pointLight2 = new PointLight(new Color(0.4,0.4,0.4,0.6));
        pointLight2.setTranslateX(1800);
        pointLight2.setTranslateY(2300);
        pointLight2.setTranslateZ(2000);
        root.getChildren().add(pointLight2);

        //俯瞰カメラ位置設定
        topViewCamera = new PerspectiveCamera(true);
        topViewCamera.setFarClip(1_000_000.0);
        topViewCamera.getTransforms().add(new Rotate(180,Rotate.Y_AXIS));
        topViewCamera.getTransforms().add(new Rotate(90,Rotate.Z_AXIS));
        topViewCamera.setTranslateX(850);
        topViewCamera.setTranslateY(1150);
        topViewCamera.setTranslateZ(3700);

        modelScene = new Scene(root,width, height, true, SceneAntialiasing.BALANCED);
        modelScene.setFill(Color.WHITE);
        modelScene.setCamera(selfViewCamera);
    }

    public WritableImage getSelfViewImage(){
        //modelScene.setCamera(selfViewCamera);
        return modelScene.snapshot(null);
    }
    public WritableImage getTopViewImage(){
        modelScene.setCamera(topViewCamera);
        return modelScene.snapshot(null);
    }

    public void release(){
    }

    public Position getSelfViewPosition(){
        return position;
    }

    public void setSelfViewPosition(Position position){
        this.position = position;

        selfViewTranslate.setX(position.x);
        selfViewTranslate.setY(position.y);
        selfViewRotateZ.setAngle(-90-position.rotation);
    }

    public void setTransform(Node node, Transform transform){
        node.getTransforms().removeAll();
        node.getTransforms().add(transform);
    }
}
