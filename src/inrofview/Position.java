package inrofview;

/**
 * Created by jimmy on 2015/06/11.
 */
public class Position {
    public double x;
    public double y;
    public double rotation;

    public Position(double x, double y, double rotation){
        this.x = x;
        this.y = y;
        this.rotation = rotation;
    }
}
