package inrofview;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.*;

import java.io.IOException;
import java.net.URL;

import javafx.scene.image.Image;
import javafx.util.Duration;
import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;

import java.text.NumberFormat;
import java.util.Map;
import java.util.ResourceBundle;

public class MainPaneController implements Initializable{
    @FXML
    private ImageView CameraView;
    @FXML
    private ImageView ModelView;
    @FXML
    private ImageView TopView;
    @FXML
    public ComboBox PortsList;
    @FXML
    public Button ReloadPortsButton;
    @FXML
    public Button ConnectSerialButton;
    @FXML
    public TextField SendMessageField;
    @FXML
    public Button SendMessageButton;
    @FXML
    public TextArea LogMessageArea;
    @FXML
    public TextField showXField;
    @FXML
    public TextField showYField;
    @FXML
    public TextField showRField;
    @FXML
    public TextField showInCalcRangeField;
    @FXML
    public Button ToggleCalcButton;
    @FXML
    public TextField showStateField;

    VideoLoader videoLoader;
    ModelManager modelManager;
    SerialManager serialManager;
    Map<String,Boolean> ports;

    Timeline timer;

    private int state;
    private boolean calcflag_manual;
    private boolean calcflag_area;
    private boolean manualmode;

    public static final double CAMERA_FOV = 34;
    public static final double CAMERA_HEIGHT = 290;
    public static final double CAMERA_ELEVATION = 65;
    public static final double IMAGE_WIDTH = 640;
    public static final double IMAGE_HEIGHT = 480;

    public static final double HEIGHT_MM_PER_DOT =
            CAMERA_HEIGHT *
                    (Math.tan(Math.toRadians(CAMERA_ELEVATION + CAMERA_FOV/2))
                            - Math.tan(Math.toRadians(CAMERA_ELEVATION - CAMERA_FOV/2)))
            / IMAGE_HEIGHT * 0.3;
    public static final double WIDTH_DEGREE_PER_DOT =
            CAMERA_FOV / IMAGE_WIDTH;

    @Override
    public void initialize(URL location, ResourceBundle resources){
        try {
            videoLoader = new VideoLoader(0);
            addLog("Camera Loaded");
        }catch (IOException e){
            e.printStackTrace();
        }

        URL url = getClass().getResource("/models/inrof-field_no-goal.obj");
        modelManager = new ModelManager(url,CAMERA_FOV, CAMERA_HEIGHT, CAMERA_ELEVATION, IMAGE_WIDTH, IMAGE_HEIGHT);
        addLog("Model Loaded");

        reloadPortList();

        setState(0);
        calcflag_manual = false;
        setIsCaluclating(false);
        manualmode = false;
        toggleCalculating();
        //setManualMode();

        timer = new Timeline(
                new KeyFrame(
                        Duration.millis(300),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event){
                                //画像取得
                                Mat frame = videoLoader.getImage();
                                if (frame != null && !frame.empty()) {
                                    //リサイズ
                                    Mat realmat = new Mat();
                                    Imgproc.resize(frame,realmat, new Size(IMAGE_WIDTH, IMAGE_HEIGHT));

                                    if (calcflag_manual && calcflag_area){
                                        Point shift = matchPosition(realmat, 2, 2, 3, 3, 400, 100);
                                        Detector.drawPhaseShift(realmat, shift);
                                    }
                                    Image modelimage = modelManager.getSelfViewImage();

                                    ModelView.setImage(modelimage);
                                    CameraView.setImage(Detector.matToImage(realmat));
                                    realmat.release();
                                    frame.release();
                                }

                                Position position = modelManager.getSelfViewPosition();
                                showXField.setText(Double.toString(position.x));
                                showYField.setText(Double.toString(position.y));
                                showRField.setText(Double.toString(position.rotation));
                                readMessage();
                            }
                        }
                )
        );
        timer.setCycleCount(Timeline.INDEFINITE);
        timer.play();
    }

    private void cameraGoFoward(double range){
        Position prev = modelManager.getSelfViewPosition();
        Position next = new Position(
                prev.x + range*Math.cos(Math.toRadians(prev.rotation)),
                prev.y + range*Math.sin(Math.toRadians(prev.rotation)),
                prev.rotation);
        modelManager.setSelfViewPosition(next);
    }

    private Point matchPosition(Mat realimage, int goTimes, int rotateTimes, double minShiftX, double minShiftY, double maxShiftX, double maxShiftY){
        Mat modelimage = Detector.imageToMat(modelManager.getSelfViewImage());
        Position prev = modelManager.getSelfViewPosition();
        Point shift = new Point(0,0);
        Point prevshift = null;

        for(int i=0; i<goTimes; i++){
            shift = Detector.getPhaseShift(modelimage, realimage);
            if(prevshift != null && Math.abs(shift.y) > Math.abs(prevshift.y)){
                modelManager.setSelfViewPosition(prev);
                shift = prevshift;
                break;
            }
            else if(Math.abs(shift.y) > maxShiftY){
                break;
            }
            else prev = modelManager.getSelfViewPosition();
            if (Math.abs(shift.y) < minShiftY) break;

            double range = shift.y * HEIGHT_MM_PER_DOT;
            cameraGoFoward(range);
            modelimage = Detector.imageToMat(modelManager.getSelfViewImage());
            prevshift = shift;
        }

        for(int i=0; i<rotateTimes; i++){
            shift = Detector.getPhaseShift(modelimage, realimage);
            if(prevshift != null && Math.abs(shift.x) > Math.abs(prevshift.x)){
                modelManager.setSelfViewPosition(prev);
                shift = prevshift;
                break;
            }
            else if(Math.abs(shift.x) > maxShiftX){
                break;
            }
            else prev = modelManager.getSelfViewPosition();
            if (Math.abs(shift.x) < minShiftX) break;

            cameraRotate(shift.x * WIDTH_DEGREE_PER_DOT);
            modelimage = Detector.imageToMat(modelManager.getSelfViewImage());
            prevshift = shift;
        }

        return shift;
    }

    private void cameraRotate(double degree){
        Position prev = modelManager.getSelfViewPosition();
        modelManager.setSelfViewPosition(new Position(
                prev.x,
                prev.y,
                prev.rotation + degree
        ));
    }

    public void addLog(String message){
        LogMessageArea.appendText(message+"\n");
        System.out.println(message);
    }

    @FXML
    public void reloadPortList(){
        ports = SerialManager.getPorts();
        PortsList.getItems().removeAll();
        for (String portname : ports.keySet())
            PortsList.getItems().add(portname);
    }
    @FXML
    public void toggleCalculating(){
        if (calcflag_manual){
            calcflag_manual = false;
            ToggleCalcButton.setText("推定開始");
        }
        else {
            calcflag_manual = true;
            ToggleCalcButton.setText("推定停止");
        }
    }

    public void sendMessage(String message){
        if (serialManager != null && serialManager.isOpened()){
            serialManager.sendMessage(message+"\n");
            addLog("Send: " + message);
        }
    }

    @FXML
    public void sendMessageManually(){
        String message = SendMessageField.getText();
        if (message != "") {
            sendMessage(message);
            SendMessageField.setText("");
        }
    }

    @FXML
    public void connect(){
        if (serialManager == null) {
            String portname = (String) PortsList.getValue();
            serialManager = new SerialManager(portname);
            addLog("Serialport("+portname+") open");
            ConnectSerialButton.setText("切断");
        }
        else{
            serialManager.close();
            serialManager = null;
            addLog("Serialport close");
            ConnectSerialButton.setText("接続");
        }
    }

    public void readMessage(){
        if(serialManager == null) return;

        for (String line : serialManager.getRecievedMessages()){
            line = line.trim().replace("\n","");
            addLog("Recieve: "+line);
            String[] splited = line.split("/");

            if(manualmode) continue;

            switch (splited[0]){
                /*
                case "getpos":
                    Position nowpos = modelManager.getSelfViewPosition();
                    sendMessage("pos/"+Double.toString(nowpos.x)+"/"+Double.toString(nowpos.y)+"/"+Double.toString(nowpos.rotation));
                    break;
                case "setpos":
                    double x = Double.parseDouble(splited[1]);
                    double y = Double.parseDouble(splited[2]);
                    double r = Double.parseDouble(splited[3]);
                    modelManager.setSelfViewPosition(new Position(x,y,r));
                    break;
                case "encalc":
                    calcflag_area = true;
                    break;
                    */
                case "done":
                    if (state == 0) {
                        setIsCaluclating(true);
                        setState(1);
                        sendMessage("done");
                    }
                    else if (state == 1){
                        Position pos = modelManager.getSelfViewPosition();

                        int targetX = 300,targetY = 2050;
                        double targetRotation = Math.toDegrees(Math.atan2(targetY - pos.y, targetX - pos.x));
                        double diffRotation = pos.rotation - targetRotation;
                        NumberFormat format = NumberFormat.getNumberInstance();
                        format.setMaximumFractionDigits(2);
                        addLog("State1 diffR: "+format.format(diffRotation));

                        if (pos.y > 1450){
                            addLog("Reach y=1450");
                            //if (pos.rotation > 120) sendMessage("right");
                            if(pos.rotation > 105) sendMessage("righth");
                            //else if(pos.rotation < 60) sendMessage("left");
                            else if(pos.rotation < 75) sendMessage("lefth");
                            else{
                                setState(2);
                                setIsCaluclating(false);
                                sendMessage("go7");
                            }
                        }
                        //else if (diffRotation > 30) sendMessage("right");
                        else if(diffRotation > 15) sendMessage("righth");
                        //else if(diffRotation < -30) sendMessage("left");
                        else if(diffRotation < -15) sendMessage("lefth");
                        else sendMessage("go4");
                    }
                    else if (state == 2){
                        sendMessage("right");
                        setState(3);
                    }
                    else if (state == 3){
                        sendMessage("right");
                        setState(4);
                    }
                    else if (state == 4){
                        sendMessage("right");
                        modelManager.setSelfViewPosition(new Position(300,2050,0));
                        setIsCaluclating(true);
                        setState(5);
                    }
                    else if (state == 5){
                        Position pos = modelManager.getSelfViewPosition();
                        //if (pos.rotation > 30) sendMessage("right");
                        if(pos.rotation > 15) sendMessage("righth");
                        //else if(pos.rotation < -30) sendMessage("left");
                        else if(pos.rotation < -15) sendMessage("lefth");
                        else{
                            setState(6);
                            //setIsCaluclating(false);
                            sendMessage("go6");
                        }
                    }
                    else if (state == 6){
                        sendMessage("goh");
                        setState(7);
                    }
                    else if (state == 7){
                        sendMessage("right");
                        setState(8);
                    }
                    else if (state == 8){
                        sendMessage("right");
                        setState(9);
                    }
                    else if (state == 9){
                        sendMessage("right");
                        modelManager.setSelfViewPosition(new Position(1340,2250,-90));
                        //setIsCaluclating(true);
                        setState(10);
                    }
                default:
                    break;
            }
        }
    }

    public void setIsCaluclating(boolean isCaluclating){
        calcflag_area = isCaluclating;
        showInCalcRangeField.setText(Boolean.toString(isCaluclating));
        addLog("IsCalculating Going: "+Boolean.toString(isCaluclating));
    }
    public void setState(int nextState){
        state = nextState;
        showStateField.setText(Integer.toString(nextState));
        addLog("State Going: "+Integer.toString(nextState));
    }
    public void setManualMode(){
        manualmode = true;
        calcflag_area = true;
        toggleCalculating();
    }
}
