package inrofview;

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import java.awt.image.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by jimmy on 2015/06/07.
 */
public class Detector {
    public static Mat getInRange(Mat src, Scalar bottom, Scalar upper){
        Mat colorRegion = new Mat();
        Mat invColorRegion = new Mat();
        Mat result = new Mat();

        Core.inRange(src, bottom, upper, colorRegion);

        return colorRegion;
    }

    public static Mat deleteColor(Mat delete_target, Mat src, Scalar bottom, Scalar upper){
        Mat colorRegion = new Mat();
        Mat inaColorRegion = new Mat();
        Mat result = new Mat();

        Core.inRange(src, bottom, upper, colorRegion);
        Core.bitwise_not(colorRegion, inaColorRegion);
        delete_target.copyTo(result, inaColorRegion);

        return result;
    }

    public static Mat imageToMat(Image image)
    {
        //BufferedImage bimage;
        //bimage = SwingFXUtils.fromFXImage(image, null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ImageIO.write(convertType(SwingFXUtils.fromFXImage(image, null),BufferedImage.TYPE_3BYTE_BGR), "bmp", baos);
            baos.close();
        }catch (IOException e){
            e.printStackTrace();
        }

        //byte[] data = convertBufferedImageToByteArray(bimage);
        //Mat mat = new Mat(bimage.getHeight(), bimage.getWidth(), CvType.CV_8UC3);
        //mat.put(0, 0, data);

        MatOfByte mb = new MatOfByte(baos.toByteArray());
        Mat mat = Imgcodecs.imdecode(mb,Imgcodecs.CV_LOAD_IMAGE_COLOR);

        return mat;
    }

    public static Image matToImage(Mat mat) {
        MatOfByte byteMat = new MatOfByte();
        Imgcodecs.imencode(".bmp", mat, byteMat);
        return new Image(new ByteArrayInputStream(byteMat.toArray()));
    }

    public static BufferedImage convertType(BufferedImage image, int type){
        BufferedImage converted = new BufferedImage(image.getWidth(), image.getHeight(),type);
        converted.getGraphics().drawImage(image,0,0,null);

        return converted;
    }

    public static MatOfKeyPoint getFeature(Mat image){
        MatOfKeyPoint keypoint = new MatOfKeyPoint();
        FeatureDetector detector = FeatureDetector.create(FeatureDetector.AKAZE);
        detector.detect(image, keypoint);

        return keypoint;
    }

    public static Mat getHoughLine(Mat image){
        Mat lines = new Mat();
        Mat grayimage = new Mat();
        Imgproc.cvtColor(image, grayimage, Imgproc.COLOR_BGR2GRAY);
        Imgproc.Canny(grayimage,grayimage, 80, 200);

        Imgproc.HoughLinesP(grayimage, lines, 2, Math.PI / 180, 180, 40, 80);

        return lines;
    }

    public static void fncDrwLine(Mat lin,Mat img) {
        double[] data;

        Point pt1 = new Point();
        Point pt2 = new Point();
        for (int i = 0; i < lin.rows(); i++){
            data = lin.get(i, 0);
            pt1.x = data[0];
            pt1.y = data[1];
            pt2.x = data[2];
            pt2.y = data[3];
            Imgproc.line(img, pt1, pt2, new Scalar(0, 255, 0), 2);
        }
    }

    public static Point getPhaseShift(Mat before, Mat after){
        Mat before_gray = new Mat();
        Mat after_gray = new Mat();
        Mat before_med = new Mat();
        Mat after_med = new Mat();
        Mat before_64f = new Mat();
        Mat after_64f = new Mat();
        Mat window = new Mat();
        Point shift;

        //グレイスケール化
        Imgproc.cvtColor(before,before_gray,Imgproc.COLOR_BGR2GRAY);
        Imgproc.cvtColor(after,after_gray,Imgproc.COLOR_BGR2GRAY);

        //メジアンフィルタ
        Imgproc.medianBlur(before_gray, before_med, 7);
        Imgproc.medianBlur(after_gray, after_med, 7);

        //64bit float化
        before_med.convertTo(before_64f, CvType.CV_64F);
        after_med.convertTo(after_64f, CvType.CV_64F);

        //窓関数生成
        Imgproc.createHanningWindow(window, before.size(), CvType.CV_64F);

        //移送限定相関法によるシフト量取得
        shift = Imgproc.phaseCorrelate(before_64f, after_64f, window, null);

        before_gray.release();
        after_gray.release();
        before_med.release();
        after_med.release();
        before_64f.release();
        after_64f.release();
        window.release();

        return shift;
    }

    public static void drawPhaseShift(Mat image, Point shift){
        Point center = new Point(image.width()/2, image.height()/2);
        Imgproc.line(image, center, new Point(center.x + shift.x, center.y + shift.y), new Scalar(0,255,0),2);
    }
}
